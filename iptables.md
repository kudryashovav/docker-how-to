#IPTABLES

Создаем правило которое поднимается после интерфейса

#/bin/bash
IPT=`which iptables`
IPS=`which ipset`

```
$IPS -q --create http_node_access nethash
$IPS -F http_node_access
$IPS -A http_node_access 10.194.15.79/32
$IPS -A http_node_access 21.33.23.43/32 
```
Так как цепочка DOCKER-USER отсутствут на момент создания правил (создается при запуске докера), то я ничего не придумал лучше как добавить правило в `ExecStartPost` в docker.service  
Проверяем если правила нет, добавляем его.
```bash
ExecStartPost=/bin/bash -c 'if [[ $(/sbin/iptables -S) != *"-A DOCKER-USER -i ens6 -m set ! --match-set http_node_access src -j DROP"* ]]; then /sbin/iptables -I DOCKER-USER -i ens6 -m set ! --match-set http_node_access src -j DROP; fi'
```