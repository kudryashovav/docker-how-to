# Docker how to
1. [Конфигурация-мониторинг памяти и CPU в Docker](https://gitlab.com/KudryashovAV/docker-how-to/blob/master/memory_cpu.md)
2. [Очистка "мусора" на нодах Docker (при наличии)](https://gitlab.com/KudryashovAV/docker-how-to/blob/master/clear.md)
3. [Изоляция контейнеров с помощью пространства имен](https://gitlab.com/KudryashovAV/docker-how-to/blob/master/namespace.md)
4. [Мониторинг Docker](https://gitlab.com/KudryashovAV/monitoring/blob/master/ReadME.md)
5. [Прибераемся в сборочноем окружении](https://gitlab.com/KudryashovAV/docker-how-to/blob/master/build_cleanup.sh)
6. [Docker Registry v2](https://gitlab.com/KudryashovAV/docker-how-to/blob/master/docker_hub.md)
7. [error while loading shared libraries: libz.so.1](https://gitlab.com/KudryashovAV/docker-how-to/blob/master/libz.so.1.md)
8. Мониторинг доступности ноды
9. [Ingress debug]( https://github.com/moby/moby/issues/36032) & https://github.com/moby/moby/issues/35082
[чтиво](https://habr.com/ru/post/108763/)
10. [docker system prune in swarm](https://gitlab.com/KudryashovAV/docker-how-to/blob/master/swarm_prune.md)
11. [macvlan](https://gitlab.com/KudryashovAV/docker-how-to/blob/master/macvlan.md)
12. [sheduling](https://github.com/docker/swarmkit/issues/308)
13. [подключение к сети docker по ssh](https://gitlab.com/KudryashovAV/docker-how-to/blob/master/sshd-tunnel.md)
```
nsenter --net=/run/docker/netns/ingress_sbox
#default
sysctl net.netfilter.nf_conntrack_tcp_timeout_time_wait
net.netfilter.nf_conntrack_tcp_timeout_time_wait = 120
```
```
docker node inspect node-ex-5 -f '{{ .Status.State }}'
```

```
sudo nsenter --net=/var/run/docker/netns/ingress_sbox cat /proc/net/nf_conntrack | grep dport=40009 | wc -l
```