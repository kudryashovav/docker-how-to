# Isolate containers with a user namespace.
- *Если вы создаете контейнер без пространства имен, то по умолчанию процессы, выполняющиеся внутри контейнера, с точки зрения хоста будут работать от имени суперпользователя. Однако мы можем создать отдельное пользовательское пространство имен*
1. Первым делом укажем user namespase remap в **/etc/docker/daemon.json**
```json
{
...
  "userns-remap": "anatoly",
...
}
```
Далее yзнаем id нашего пользователя
```shell
[root@cen-node-7 ~]# id -u anatoly
1000

```
И добавляем в  [**/etc/subuid** и **/etc/subgid**](https://gitlab.com/KudryashovAV/docker-how-to/blob/master/subuid_subgid.md)
```shell
echo "anatoly:1000:65536" >> /etc/subuid
echo "anatoly:1000:65536" >> /etc/subgid
```

Если используется симейство ОС RHEL то по умолчанию namespaces отключены на уровне ядра. Делаем следующее->
```shell
grubby --args="namespace.unpriv_enable=1 user_namespace.enable=1" --update-kernel="$(grubby --default-kernel)"
echo "user.max_user_namespaces=15076" >> /etc/sysctl.conf
reboot
```

- Криминальное чтиво:
[1](https://docs.docker.com/engine/security/userns-remap/), [2](https://habr.com/company/southbridge/blog/339126/), [3](http://vasilisc.com/lxc-1-0-unprivileged-containers)
