#/bin/bash

IPS=`which ipset`
 
$IPS -q --create node_access nethash
$IPS -F node_access 
$IPS -A node_access 10.59.143.0/23
$IPS -A node_access 100.194.159.99


/sbin/iptables -t nat -F PREROUTING
/sbin/iptables -t nat -F POSTROUTING
/sbin/iptables -t nat -F OUTPUT
/sbin/iptables -t nat -N DOCKER-INGRESS

countINGRESS_RETURN=`/sbin/iptables -t nat -S DOCKER-INGRESS | grep -c '\-A DOCKER-INGRESS \-j RETURN'`
if [ "${countINGRESS_RETURN}" -eq "0" ]; then
  /sbin/iptables -t nat -A DOCKER-INGRESS -j RETURN
fi

i=0
countGRAY_NET_RETURN=`/sbin/iptables -t nat -S DOCKER-INGRESS | tail -n +3 | grep -c '\-\-match\-set node_access src \-j RETURN'`
while [ ${countGRAY_NET_RETURN} -gt 0 ] && [ ${i} -lt 5 ]; do
    i=$((i+1))
    /sbin/iptables -t nat -D DOCKER-INGRESS -m set ! --match-set node_access src -j RETURN
    countGRAY_NET_RETURN=`/sbin/iptables -t nat -S DOCKER-INGRESS | tail -n +3 | grep -c '\-\-match\-set node_access src \-j RETURN'`
done

countGRAY_NET_RETURN=`/sbin/iptables -t nat -S DOCKER-INGRESS | head -n 2 | grep -c '\-\-match\-set node_access src  \-j RETURN'`
if [ "${countGRAY_NET_RETURN}" -eq "0" ]; then
    /sbin/iptables -t nat -I DOCKER-INGRESS 1 -m set ! --match-set node_access src -j RETURN
fi

/sbin/iptables -t nat -A PREROUTING -m addrtype --dst-type LOCAL -j DOCKER-INGRESS

/sbin/iptables -t nat -A POSTROUTING -s 172.0.0.0/8 -o eth+ -j MASQUERADE

/sbin/iptables -t nat -A OUTPUT -m addrtype --dst-type LOCAL -j DOCKER-INGRESS

/sbin/iptables -A INPUT -s 172.0.0.0/8 -i eth0 -j DROP
/sbin/iptables -A INPUT -s 172.0.0.0/8 -i eth1 -j DROP
/sbin/iptables -A INPUT -s 172.0.0.0/8 -j ACCEPT
/sbin/iptables -A FORWARD -s 172.0.0.0/8 -j ACCEPT
/sbin/iptables -A FORWARD -d 172.0.0.0/8 -j ACCEPT