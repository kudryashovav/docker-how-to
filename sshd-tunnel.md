Чтобы подключится напрямую к контейнеру можно использовать [macvlan](https://gitlab.com/KudryashovAV/docker-how-to/blob/master/macvlan.md) или использовать ssh-tunnel.
Создадим образ
```
FROM alpine

ADD docker-entrypoint.sh /usr/local/bin

RUN apk add --update openssh \
    && rm  -rf /tmp/* /var/cache/apk/* /etc/ssh/ssh_host_rsa_key /etc/ssh/ssh_host_dsa_key \
    && sed -i s/^#PasswordAuthentication\ yes/PasswordAuthentication\ no/ /etc/ssh/sshd_config \ # Запрещаем парольную авторизацию
    && sed -i s/^AllowTcpForwarding\ no/AllowTcpForwarding\ yes/ /etc/ssh/sshd_config \     # Порт форвардинг для туннелирования
    && mkdir /root/.ssh \
    && chmod 0700 /root/.ssh \
    && > /etc/motd \ # Убираем приветствие
    && passwd -u root \
    && sed -i 's/ash/true/g' /etc/passwd #Запрещаем shell, чтобы нельзя было подключится. Если нужно подключаться нужно выпилить эту строку

EXPOSE 22
ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["/usr/sbin/sshd", "-D", "-e"] #-D запускаем как демон, -e логируем в stderr
```
ентрипойнт скрипт, производим проверку смонтирован ли файл с публичными ключами или переданны они с помощью переменных.  
Если переменные создаем файл. если передается файл, то владелец должен быть рут  
Генерим rsa/dsa
```sh
#!/bin/sh

if [ ! -f "/root/.ssh/authorized_keys" ] && [ -z "${AUTHORIZED_KEYS}" ]; then
        echo "Need your ssh public key as AUTHORIZED_KEYS env variable or mount public key to /root/.ssh/authorized_keys. Abnormal exit ..."
        exit 1
fi

if [ ! -z "${AUTHORIZED_KEYS}" ]; then
        echo "Populating /root/.ssh/authorized_keys with the value from AUTHORIZED_KEYS env variable ..."
        echo "${AUTHORIZED_KEYS}" > /root/.ssh/authorized_keys
fi

if [ ! -f "/etc/ssh/ssh_host_rsa_key" ]; then
        # generate fresh rsa key
        ssh-keygen -f /etc/ssh/ssh_host_rsa_key -N '' -t rsa
fi
if [ ! -f "/etc/ssh/ssh_host_dsa_key" ]; then
        # generate fresh dsa key
        ssh-keygen -f /etc/ssh/ssh_host_dsa_key -N '' -t dsa
fi

#prepare run dir
if [ ! -d "/var/run/sshd" ]; then
  mkdir -p /var/run/sshd
fi

exec "$@"
```
Использование:
```bash
docker run -d -v $(pwd)/authorized_keys:/root/.ssh/authorized_keys sshd-tunnel
```
подключаемя к контейнеру с sshd `root@172.17.0.4`
-L перенаправляем локальный порт 8082 на контейнер 172.17.0.5:80
-N позволяет не подключаться к оболочке
```bash
ssh -N root@172.17.0.4 -L 8082:172.17.0.5:80
```


