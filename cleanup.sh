#!/bin/bash
GRE='\033[0;32m'
NC='\033[0m'
# Необходимо указать имя проекта имя репозитория, а так же количество тегов которые необходимо оставить.
project='devops'
repo='prometheus'
count=2

function repolist {
python -c '
import requests, json
repos = json.loads(requests.get("http://127.0.0.1:5000/v2/_catalog?n=2000", verify=False).text)
taglist=[]
emptyrepos=[]
for repo in repos["repositories"]:
    tags = json.loads(requests.get("http://127.0.0.1:5000/v2/" + repo.replace("/", "%2F") + "/tags/list", verify=False).text)
    if tags["tags"] is None:
       emptyrepos.append(repo)	
    else:
        for tag in tags["tags"]:
	    print(repo + ":" + tag)
            taglist.append(repo + ":" + tag)'
#print (emptyrepos)
#print (taglist)
}

echo -e "${GRE}Всего тегов:${NC}" $(repolist | grep $repo | sort | wc -l)

if [ $(repolist | grep $repo | sort | cut -d: -f2 | head -n -$count | wc -l) -ne 0 ]
then
  echo -e "${GRE}Тегов для удаления:${NC}" $(repolist | grep $repo | sort | cut -d: -f2 | head -n -$count | wc -l)
  repolist | grep $repo | sort | cut -d: -f2 | head -n -$count


  read -p "Удалить теги(Yy)? $cr" -n 1 -r
  echo -e $NC
        if [[ $REPLY =~ ^[Yy]$ ]]
        then
          for tag in $(repolist | grep $repo | sort | cut -d: -f2 | head -n -$count);do
 	  curl -s -X DELETE "http://127.0.0.1:5000/v2/${project}%2F${repo}/manifests/$(curl -s "http://127.0.0.1:5000/v2/${project}%2F${repo}/manifests/${tag}" -H 'Accept: application/vnd.docker.distribution.manifest.v2+json' -I | grep Docker-Content-Digest | awk '{print $2}' | sed 's/\r//g')"
	  done
          fi
else
  echo -e "${GRE}Теги для удаления отсутствуют${NC}"
fi
