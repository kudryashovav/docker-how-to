1. Drain node.
```bash
sudo docker node update --availability drain worker2
```
2. Verify No containers are on drained node.
```bash
sudo docker ps #on worker2
```
3. Upgrade docker engine.

4. Check node in swarm manager.
```bash
sudo docker node ls
```
5. Change availablity to Active.
```bash
sudo docker node update --availability Active
```
Repeat the steps for other nodes.

If one manager
promote worker nodes

demote manager

update