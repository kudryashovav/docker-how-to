1. Решение на уровне демона для всех контейнеров
плюсы: применяется на все вновь созданные контейнеры
минусы: перезапуск демона, если контейнер создается не сервисом пересоздать контейнер
- через json
```json
{ 
  "log-driver": "json-file", #можно использовать другие драйверы логов например gelf и отдавать логи в Graylog
  "log-opts": {
   "max-size": "10m",
   "max-file": "10" 
 }
}
```
- через cat /etc/systemd/system/docker.service
```
[Service]
ExecStart=/usr/bin/dockerd -H fd:// --log-driver=syslog --log-opt syslog-address=udp://109.194.151.80:514 --log-opt tag={{.Name}}/{{.ID}}
```

2. На момент запуска контейнера
```shell
$ docker run --log-driver json-file --log-opt max-size=10m --log-opt max-file=10 <Image>
```
3. В service create/update
```shell
--log-driver json-file --log-opt max-size=10m --log-opt max-file=10
```
4. В compose
```yml
logging:
  driver: "json-file"
  options:
    max-size: "10m"
    max-file: "10"
```    
5. Без пересоздания контейнера
```shell
systemctl stop docker
vim /var/lib/docker/containers/9438e144f5fa9261ed5eedec3156e7b9bd373c22aaafac01065bb6ca52b059f2/hostconfig.json
```
Правим конфиг->
```json
"LogConfig":{"Type":"json-file","Config":{"max-file":"3","max-size":"10m"}}
```
```shell
systemctl start docker
```